package com.nexsoft;

import java.util.Scanner;

public class Calendar {
    public static void main(String[] args){

//        Template
//        {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
//        {"\t","\t","\t","\t","\t","\t","\t"},
//        {"\t","\t","\t","\t","\t","\t","\t"},
//        {"\t","\t","\t","\t","\t","\t","\t"},
//        {"\t","\t","\t","\t","\t","\t","\t"},
//        {"\t","\t","\t","\t","\t","\t","\t"},
//        {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}
        int tahun;
        System.out.println("Masukkan Tahun : ");
        Scanner keyboard =  new Scanner(System.in);
        tahun = keyboard.nextInt();
        System.out.println("\nKalender tahun " + tahun);

        System.out.println("Menampilkan 1 tahun");

        String[][] Januari = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t", "--\t", "--\t","1\t","2\t", "3\t","4\t"},
                {"5\t","6\t","7\t","8\t","9\t","10\t","11\t"},
                {"12\t","13\t","14\t","15\t","16\t","17\t","18\t"},
                {"19\t","20\t","21\t","22\t","23\t","24\t","25\t"},
                {"26\t","27\t","28\t","29\t","30\t","31\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}
        };

        System.out.println("===== Bulan January =====");
        for (int i = 0; i < Januari.length; i++) {
            System.out.print(Januari[i][0]);
            System.out.print(Januari[i][1]);
            System.out.print(Januari[i][2]);
            System.out.print(Januari[i][3]);
            System.out.print(Januari[i][4]);
            System.out.print(Januari[i][5]);
            System.out.print(Januari[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        String[][] Februari = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t", "--\t", "--\t","--\t","--\t","--\t","1\t"},
                {"2\t","3\t","4\t","5\t","6\t","7\t","8\t"},
                {"9\t","10\t","11\t","12\t","13\t","14\t","15\t"},
                {"16\t","17\t","18\t","19\t","20\t","21\t","22\t"},
                {"23\t","24\t","25\t","26\t","27\t","28\t","29\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}
        };


        System.out.println("===== Bulan February =====");
        for (int i = 0; i < Februari.length; i++) {
            System.out.print(Februari[i][0]);
            System.out.print(Februari[i][1]);
            System.out.print(Februari[i][2]);
            System.out.print(Februari[i][3]);
            System.out.print(Februari[i][4]);
            System.out.print(Februari[i][5]);
            System.out.print(Februari[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan March =====");
        String[][] Maret = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"1\t","2\t","3\t","4\t","5\t","6\t","7\t"},
                {"8\t","9\t","10\t","11\t","12\t","13\t","14\t"},
                {"15\t","16\t","17\t","18\t","19\t","20\t","21\t"},
                {"22\t","23\t","24\t","25\t","26\t","27\t","28\t"},
                {"29\t","30\t","31\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < Maret.length; i++) {
            System.out.print(Maret[i][0]);
            System.out.print(Maret[i][1]);
            System.out.print(Maret[i][2]);
            System.out.print(Maret[i][3]);
            System.out.print(Maret[i][4]);
            System.out.print(Maret[i][5]);
            System.out.print(Maret[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan April =====");
        String[][] April = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","--\t","--\t","1\t","2\t","3\t","4\t"},
                {"5\t","6\t","7\t","8\t","9\t","10\t","11\t"},
                {"12\t","13\t","14\t","15\t","16\t","17\t","18\t"},
                {"19\t","20\t","21\t","22\t","23\t","24\t","25\t"},
                {"26\t","27\t","28\t","29\t","30\t","\t","\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}
        };

        for (int i = 0; i < April.length; i++) {
            System.out.print(April[i][0]);
            System.out.print(April[i][1]);
            System.out.print(April[i][2]);
            System.out.print(April[i][3]);
            System.out.print(April[i][4]);
            System.out.print(April[i][5]);
            System.out.print(April[i][6]);
            System.out.println();
        }

        System.out.println("======= Bulan May =======");
        String[][] Mei = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","--\t","--\t","--\t","--\t","1\t","2\t"},
                {"3\t","4\t","5\t","6\t","7\t","8\t","9\t"},
                {"10\t","11\t","12\t","13\t","14\t","15\t","16\t"},
                {"17\t","18\t","19\t","20\t","21\t","22\t","23\t"},
                {"24\t","25\t","26\t","27\t","28\t","29\t","30\t"},
                {"31\t","--\t","--\t","--\t","--\t","--\t","--\t"}
        };

        for (int i = 0; i < Mei.length; i++) {
            System.out.print(Mei[i][0]);
            System.out.print(Mei[i][1]);
            System.out.print(Mei[i][2]);
            System.out.print(Mei[i][3]);
            System.out.print(Mei[i][4]);
            System.out.print(Mei[i][5]);
            System.out.print(Mei[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan June =====");
        String[][] Juni = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","1\t","2\t","3\t","4\t","5\t","6\t"},
                {"7\t","8\t","9\t","10\t","11\t","12\t","13\t"},
                {"14\t","15\t","16\t","17\t","18\t","19\t","20\t"},
                {"21\t","22\t","23\t","24\t","25\t","26\t","27\t"},
                {"28\t","29\t","30\t","--\t","--\t","--\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < Juni.length; i++) {
            System.out.print(Juni[i][0]);
            System.out.print(Juni[i][1]);
            System.out.print(Juni[i][2]);
            System.out.print(Juni[i][3]);
            System.out.print(Juni[i][4]);
            System.out.print(Juni[i][5]);
            System.out.print(Juni[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan July =====");
        String[][] Juli = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t", "--\t", "--\t","1\t","2\t", "3\t","4\t"},
                {"5\t","6\t","7\t","8\t","9\t","10\t","11\t"},
                {"12\t","13\t","14\t","15\t","16\t","17\t","18\t"},
                {"19\t","20\t","21\t","22\t","23\t","24\t","25\t"},
                {"26\t","27\t","28\t","29\t","30\t","31\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < Juli.length; i++) {
            System.out.print(Juli[i][0]);
            System.out.print(Juli[i][1]);
            System.out.print(Juli[i][2]);
            System.out.print(Juli[i][3]);
            System.out.print(Juli[i][4]);
            System.out.print(Juli[i][5]);
            System.out.print(Juli[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        String[][] Agustus = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t", "--\t", "--\t","--\t","--\t","--\t","1\t"},
                {"2\t","3\t","4\t","5\t","6\t","7\t","8\t"},
                {"9\t","10\t","11\t","12\t","13\t","14\t","15\t"},
                {"16\t","17\t","18\t","19\t","20\t","21\t","22\t"},
                {"23\t","24\t","25\t","26\t","27\t","28\t","29\t"},
                {"30\t","31\t","--\t","--\t","--\t","--\t","--\t"}
        };

        System.out.println("===== Bulan Agust =====");
        for (int i = 0; i < Agustus.length; i++) {
            System.out.print(Agustus[i][0]);
            System.out.print(Agustus[i][1]);
            System.out.print(Agustus[i][2]);
            System.out.print(Agustus[i][3]);
            System.out.print(Agustus[i][4]);
            System.out.print(Agustus[i][5]);
            System.out.print(Agustus[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan September =====");
        String[][] September = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","--\t","1\t","2\t","3\t","4\t","5\t"},
                {"6\t","7\t","8\t","9\t","10\t","11\t","12\t"},
                {"13\t","14\t","15\t","16\t","17\t","18\t","19\t"},
                {"20\t","21\t","22\t","23\t","24\t","25\t","26\t"},
                {"27\t","28\t","29\t","30\t","--\t","--\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < September.length; i++) {
            System.out.print(September[i][0]);
            System.out.print(September[i][1]);
            System.out.print(September[i][2]);
            System.out.print(September[i][3]);
            System.out.print(September[i][4]);
            System.out.print(September[i][5]);
            System.out.print(September[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan October =====");
        String[][] Oktober = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","--\t","--\t","--\t","1\t","2\t","3\t"},
                {"4\t","5\t","6\t","7\t","8\t","9\t","10\t"},
                {"11\t","12\t","13\t","14\t","15\t","16\t","17\t"},
                {"18\t","19\t","20\t","21\t","22\t","23\t","24\t"},
                {"25\t","26\t","27\t","28\t","29\t","30\t","31\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < Oktober.length; i++) {
            System.out.print(Oktober[i][0]);
            System.out.print(Oktober[i][1]);
            System.out.print(Oktober[i][2]);
            System.out.print(Oktober[i][3]);
            System.out.print(Oktober[i][4]);
            System.out.print(Oktober[i][5]);
            System.out.print(Oktober[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan November =====");
        String[][] November = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"1\t","2\t","3\t","4\t","5\t","6\t","7\t"},
                {"8\t","9\t","10\t","11\t","12\t","13\t","14\t"},
                {"15\t","16\t","17\t","18\t","19\t","20\t","21\t"},
                {"22\t","23\t","24\t","25\t","26\t","27\t","28\t"},
                {"29\t","30\t","--\t","--\t","--\t","--\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < November.length; i++) {
            System.out.print(November[i][0]);
            System.out.print(November[i][1]);
            System.out.print(November[i][2]);
            System.out.print(November[i][3]);
            System.out.print(November[i][4]);
            System.out.print(November[i][5]);
            System.out.print(November[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

        System.out.println("===== Bulan December =====");
        String[][] Desember = {
                {"M\t","S\t","S\t","R\t","K\t","J\t","S\t"},
                {"--\t","--\t","1\t","2\t","3\t","4\t","5\t"},
                {"6\t","7\t","8\t","9\t","10\t","11\t","12\t"},
                {"13\t","14\t","15\t","16\t","17\t","18\t","19\t"},
                {"20\t","21\t","22\t","23\t","24\t","25\t","26\t"},
                {"27\t","28\t","29\t","30\t","31\t","--\t","--\t"},
                {"--\t","--\t","--\t","--\t","--\t","--\t","--\t"}

        };

        for (int i = 0; i < Desember.length; i++) {
            System.out.print(Desember[i][0]);
            System.out.print(Desember[i][1]);
            System.out.print(Desember[i][2]);
            System.out.print(Desember[i][3]);
            System.out.print(Desember[i][4]);
            System.out.print(Desember[i][5]);
            System.out.print(Desember[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

//------------------------------------------------------------------------

        System.out.println("\nMenampilkan 1 bulan");
        System.out.println("===== Bulan September =====");
        for (int i = 0; i < September.length; i++) {
            System.out.print(September[i][0]);
            System.out.print(September[i][1]);
            System.out.print(September[i][2]);
            System.out.print(September[i][3]);
            System.out.print(September[i][4]);
            System.out.print(September[i][5]);
            System.out.print(September[i][6]);
            System.out.println();
        }
        System.out.println("==========================");

//------------------------------------------------------------------------
        System.out.println("\nMenampilkan 1 minggu");
        System.out.println("===== Kalender Bulan Mei Tahun 2020 Minggu Ke-3 =====");
        System.out.println("---------------------------------");
        System.out.println("|\t" + Mei[0][0] + Mei[0][1] + Mei[0][2] + Mei[0][3] + Mei[0][4] + Mei[0][5] + Mei[0][6] + "|");
        System.out.println("---------------------------------");
        System.out.println("|\t" + Mei[3][0] + Mei[3][1] + Mei[3][2] + Mei[3][3] + Mei[3][4] + Mei[3][5] + Mei[3][6] + "|");
        System.out.println("---------------------------------");
    }
}
