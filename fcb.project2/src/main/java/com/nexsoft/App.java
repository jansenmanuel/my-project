package com.nexsoft;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("---------------------------------");
        System.out.println("Jansen Manuel - Nexsoft");
        System.out.println("---------------------------------");
        System.out.println("Berikut adalah tugas Array List :");

//        Array List 1
        ArrayList<String> alphabet1 = new ArrayList<>();
        alphabet1.add("A");
        alphabet1.add("B");
        alphabet1.add("C");
        alphabet1.add("D");

//        Array List 2
        ArrayList<String> alphabet2 = new ArrayList<>();
        alphabet2.add("C");
        alphabet2.add("D");
        alphabet2.add("E");
        alphabet2.add("F");

        ArrayList<String> newAlphabet1 = new ArrayList<>(alphabet1); // newAlphabet1 mengambil data dari aplhabet1
        newAlphabet1.addAll(alphabet2); // newAlphabet1 menambahkan semua data dari alphabet2

        ArrayList<String> newAlphabet2 = new ArrayList<>(alphabet1); // newAlphabet2 mengambil data dari aplhabet1
        newAlphabet2.retainAll(alphabet2); // newAlphabet2 menghapus semua data dari alphabet2 yang berbeda

        newAlphabet1.removeAll(newAlphabet2); // newAlphabet1 menghapus semua data dari newAlphabet2
        alphabet1.removeAll(alphabet1); // aplphabet1 menghapus semua data dari alphabet1
        alphabet1.addAll(newAlphabet1); // aplphabet1 menambahkan semua data dari newAlphabet1
        System.out.println("Huruf : " + alphabet1); // memunculkan data dari alphabet1
        System.out.println("---------------------------------");
    }
}
