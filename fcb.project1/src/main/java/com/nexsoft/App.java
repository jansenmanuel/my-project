package com.nexsoft;
import com.nexsoft.test.Car;

/**
 * Hello world!
 *
 */
public class App extends Vehicle
{
    App(int initTotal) {
        super(initTotal);
    }

    public static void main(String[] args )
    {
        firstRun();

        System.out.println( "Belajar Java Bagian Pertama: \n" );
        System.out.println("---------------------------------");

//        Vehicle myCar = new Vehicle();
//        myCar.type();
//        myCar.fuel();

        MyCar myNewCar = new MyCar(3);
        myNewCar.detail();

        Car myDetail = new Car();
        myDetail.transmission();
    }

    public static void firstRun()
    {
        System.out.println("Main first run");
    }
}
