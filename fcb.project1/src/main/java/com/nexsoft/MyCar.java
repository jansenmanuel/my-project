package com.nexsoft;

public class MyCar extends Vehicle {
    private int carTotal;

    MyCar(int initTotal){
        super(initTotal);
//        this = "MyCar"
        this.carTotal = initTotal;
    }

    public void detail(){
        MyCar myNewCar = new MyCar(this.carTotal);
        myNewCar.type();
        super.fuel();
        myNewCar.fuel();
        myNewCar.color();
        System.out.println("Total Car   : " + myNewCar.getTotal());
//        Logic
        myNewCar.setTotal(10);
        System.out.println("Total Car   : " + myNewCar.getTotal());
    }

    public void fuel(){
        System.out.println("Fuel    : Solar");
    }

    public void color(){
        System.out.println("Color   : Black");
    }

}
