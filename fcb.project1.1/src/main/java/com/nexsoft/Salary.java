package com.nexsoft;

public class Salary extends ParkingCard{
    private int salary;

//    Setter Salary
    public void setSalary(int inputSalary){
        this.salary = inputSalary;
    }

//    Getter Salary
    public int getSalary(){
        return this.salary * super.getIdCard();
    }
}
