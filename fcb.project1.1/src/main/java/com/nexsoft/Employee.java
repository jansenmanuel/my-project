package com.nexsoft;

public class Employee extends Salary {
//    Pemanggilan Data dari Super Class (Parent Class)
    Employee(int inputIdCard, String inputName, int inputIdParkingCard, int inputSalary) {
        super.setIdCard(inputIdCard);
        super.setName(inputName);
        super.setIdParkingCard(inputIdParkingCard);
        super.setSalary(inputSalary);
    }

//    Pemunculan Data Employee
    public void showEmployee() {
        System.out.println("ID Card Number  : " + super.getIdCard());
        System.out.println("Employee Name   : " + super.getName());
        System.out.println("ID Parking Card : " + super.getIdParkingCard());
        System.out.println("Salary          : " + "Rp." + super.getSalary());

    }
}
