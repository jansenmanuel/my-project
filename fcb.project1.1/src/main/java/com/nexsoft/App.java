package com.nexsoft;

public class App extends Employee
{
    //    Constructor
    App(int inputIdCard, String inputName, int inputIdParkingCard, int inputSalary) {
        super(inputIdCard, inputName, inputIdParkingCard, inputSalary);
    }

    public static void main(String[] args )
    {
        System.out.println("\n");
        System.out.println("Jansen Manuel - Nextsoft");
        System.out.println("Berikut ini adalah data-data pegawai Nexsoft: ");

//        Data yang diinput
        Employee e1 = new Employee(1, "Karyawan 1", 101, 3000000);
        Employee e2 = new Employee(2, "Karyawan 2", 102, 3000000);
        Employee e3 = new Employee(3, "Karyawan 2", 103, 3000000);
        Employee e4 = new Employee(4, "Manager 1", 104, 10000000);
        Employee e5 = new Employee(5, "Manager 2", 105, 10000000);

//        Pemunculan Data (Output)
        System.out.println("----------------------------------");
        e1.showEmployee();
        System.out.println("----------------------------------");
        e2.showEmployee();
        System.out.println("----------------------------------");
        e3.showEmployee();
        System.out.println("----------------------------------");
        e4.showEmployee();
        System.out.println("----------------------------------");
        e5.showEmployee();
        System.out.println("----------------------------------");
    }

}
