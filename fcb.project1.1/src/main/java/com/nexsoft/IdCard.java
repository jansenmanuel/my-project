package com.nexsoft;

public class IdCard {
    private int idCardNumber;
    private String name;

//    Setter ID Card
    public void setIdCard(int inputIdCard){
        this.idCardNumber =  inputIdCard;
    }

//    Getter ID Card
    public int getIdCard() {
        return this.idCardNumber;
    }

//    Setter Name
    public void setName(String inputName){
        this.name = inputName;
    }

//    Getter Name
    public String getName() {
        return this.name;
    }
}