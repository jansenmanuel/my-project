package com.nexsoft;

import java.sql.*;

public class Conn {
    public void runSQL(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");

            Statement stmt = myConn.createStatement();

            ResultSet res = stmt.executeQuery("SELECT * FROM karyawan");

            System.out.println("================ Table Karyawan ================");
            while (res.next()){
                System.out.println("================================================");
                System.out.println("NIK\t\t\t\t\t: " + res.getString(1));
                System.out.println("Nama\t\t\t\t: " + res.getString(2));
                System.out.println("Tempat / Tgl Lahir\t: " + res.getString(3));
                System.out.println("Jenis Kelamin\t\t: " + res.getString(4));
                System.out.println("Alamat\t\t\t\t: " + res.getString(5));
                System.out.println("Agama\t\t\t\t: " + res.getString(6));
                System.out.println("================================================\n");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Conn sql = new Conn();
        sql.runSQL();
    }

}
