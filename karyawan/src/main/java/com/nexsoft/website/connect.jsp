<%@ page import = "java.sql.*"  %>
<%
    String nik = request.getParameter("nik");
    String nama = request.getParameter("nama");
    String ttl = request.getParameter("ttl");
    String jk = request.getParameter("jk");
    String alamat = request.getParameter("alamat");
    String agama = request.getParameter("agama");

    try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
        PreparedStatement ps = conn.prepareStatement("INSERT INTO karyawan(nik, nama, ttl, jk, alamat, agama) VALUES(?, ?, ?, ?, ?, ?);");
        ps.setString(1, nik);
        ps.setString(2, nama);
        ps.setString(3, ttl);
        ps.setString(4, jk);
        ps.setString(5, alamat);
        ps.setString(6, agama);
        int x = ps.executeUpdate();
        if (x > 0) {
            out.println("Data berhasil ditambahkan...");
        } else {
            out.println("Data gagal ditambahkan...");
        }
    } catch(Exception e){
        out.println(e);
    }
%>