package com.nexsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository karyawanRepository;

    public Karyawan saveEmployee(Karyawan karyawan) {
        return karyawanRepository.save(karyawan);
    }

    public List<Karyawan> saveEmployees(List<Karyawan> karyawans) {
        return karyawanRepository.saveAll(karyawans);
    }

    public List<Karyawan> getEmployees() {
        return karyawanRepository.findAll();
    }

    public Karyawan getEmployeeById(int id) {
        return karyawanRepository.findById(id).orElse(null);
    }

    public String deleteEmployee(int id) {
        karyawanRepository.deleteById(id);
        return "Data Employee Berhasil Dihapus";
    }
}
