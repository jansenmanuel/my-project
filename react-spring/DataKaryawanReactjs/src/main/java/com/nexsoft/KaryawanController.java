package com.nexsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class KaryawanController {
    @Autowired
    private KaryawanService karyawanService;

    @PostMapping("/addEmployee")
    public Karyawan addEmployee(@RequestBody Karyawan karyawan)
    {
        return karyawanService.saveEmployee(karyawan);
    }

    @PostMapping("/addEmployees")
    public List<Karyawan> addEmployees(@RequestBody List<Karyawan> karyawans)
    {
        return karyawanService.saveEmployees(karyawans);
    }

    @GetMapping("/employees")
    public List<Karyawan> findAllEmployees() {
        return karyawanService.getEmployees();
    }

    @GetMapping("/employeeById/{id}")
    public Karyawan findEmployeeById(@PathVariable int id){
        return karyawanService.getEmployeeById(id);
    }

    @PutMapping("/update/{id}")
    public Karyawan updateEmployee(@RequestBody Karyawan karyawan){
        return karyawanService.saveEmployee(karyawan);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable int id)
    {
        return karyawanService.deleteEmployee(id);
    }
}
