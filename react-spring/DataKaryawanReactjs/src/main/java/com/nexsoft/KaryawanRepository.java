package com.nexsoft;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KaryawanRepository extends JpaRepository <Karyawan, Integer> {
}
