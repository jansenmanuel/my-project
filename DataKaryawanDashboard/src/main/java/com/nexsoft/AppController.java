package com.nexsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AppController {
    @Autowired
    private KaryawanService service;

    @RequestMapping("/")
    public String viewHomePage(Model model) {
        List<Karyawan> karyawanList = service.listAll();
        model.addAttribute("karyawanList", karyawanList);

        return "index";
    }

    @RequestMapping("/new")
    public String showNewKaryawanForm(Model model) {
        Karyawan karyawan = new Karyawan();
        model.addAttribute("karyawan", karyawan);

        return "new_karyawan";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveKaryawan(@ModelAttribute("karyawan") Karyawan karyawan) {
        service.save(karyawan);

        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditKaryawanForm(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_karyawan");

        Karyawan karyawan = service.get(id);
        mav.addObject("karyawan", karyawan);

        return mav;
    }

    @RequestMapping("/delete/{id}")
    public String deleteKaryawan(@PathVariable(name = "id") int id) {
        service.delete(id);

        return "redirect:/";
    }

    @RequestMapping("/detail/{id}")
    public String viewDetailKaryawan(Model model, @PathVariable(name = "id") int id) {
        Karyawan karyawanList = service.get(id);
        model.addAttribute("karyawanList", karyawanList);

        return "detail_karyawan";
    }

}
