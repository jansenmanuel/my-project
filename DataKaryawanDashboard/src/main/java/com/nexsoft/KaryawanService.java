package com.nexsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository repo;

    public List<Karyawan> listAll() {
        return repo.findAll();
    }

    public void save(Karyawan karyawan) {
        repo.save(karyawan);
    }

    public Karyawan get(int  id) {
        return repo.findById(id).get();
    }

    public void delete(int id) {
        repo.deleteById(id);
    }
}
