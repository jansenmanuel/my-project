let input1DOM = document.getElementById("input1")
let input2DOM = document.getElementById("input2")

function tambah() {
    let hasilTambah = parseFloat(input1DOM.value) + parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilTambah
}

function kurang() {
    let hasilKurang = parseFloat(input1DOM.value) - parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilKurang
}

function kali() {
    let hasilKali = parseFloat(input1DOM.value) * parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilKali
}

function bagi() {
    let hasilBagi = parseFloat(input1DOM.value) / parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilBagi
}

function modulus() {
    let hasilModulus = parseFloat(input1DOM.value) % parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilModulus
}

function pangkat() {
    let hasilPangkat = parseFloat(input1DOM.value) ** parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilPangkat
}

function akar() {
    let hasilAkar = Math.sqrt(parseFloat(input1DOM.value))
    document.getElementById('hasil').value = hasilAkar
}